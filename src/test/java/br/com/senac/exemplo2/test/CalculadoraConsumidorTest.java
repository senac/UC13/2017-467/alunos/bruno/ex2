
package br.com.senac.exemplo2.test;

import br.com.senac.exemplo2.CalculadoraConsumidor;
import org.junit.Test;
import static org.junit.Assert.*;


public class CalculadoraConsumidorTest {
    
    public CalculadoraConsumidorTest() { 
        
    }
    
    @Test
    public void ValorFinalConsumidor (){
        CalculadoraConsumidor calculadora = new CalculadoraConsumidor();
        double valorCusto = 20000;
        
        double resultado = calculadora.calcular(valorCusto);
        
        assertEquals(34600,resultado, 0.01);
        
    }
    
}
