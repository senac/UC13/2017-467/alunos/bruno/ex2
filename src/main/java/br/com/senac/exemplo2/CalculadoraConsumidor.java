
package br.com.senac.exemplo2;

public class CalculadoraConsumidor {
    
    private final double PORCENTAGEM_DISTRIBUIDORA = 0.28;
    private final double IMPOSTO = 0.45;
    
    public double calcular(double valor){
     double imposto = valor * IMPOSTO;
     double lucro = valor * PORCENTAGEM_DISTRIBUIDORA;
     double valorFinal = valor + imposto + lucro;
     
     return valorFinal;
        
    }
}
